# Docs for UPC++ gitlab-ci.yml

## IMPORTANT NOTICES

* DO NOT: push any untrusted code
    * Code (including shell code in configure and Makefiles) are run on
      at least some "runners" as a normal user without the benefit of
      containerization such as Docker.
    * On Theta, CI jobs can run batch jobs charged against our allocation.
* DO: push only to your "user/" namespace.
    * Assuming use of a remote named 'alcf' and a Bitbucket user AnnExample:  
      `git push alcf HEAD:annexample/check/issue-9876`

### Automated triggers based on branch names as follows:

* Branch name == `develop`
    * Runs CI at level of `make dev-check` on every push
* Branch name contains the string `/dev-check/`
    * Runs CI at level of `make dev-check` on every push
* Branch name contains the string `/check/`
    * Runs CI at level of `make check` on every push
* All others
    * No CI is triggered, but manual triggers are still possible (see below)

### Results reporting to Bitbucket

* Build results for 'develop' are automatically reported to
  `https://bitbucket.org/berkeleylab/upcxx`
* Reports use a key of `$CI_JOB_NAME.(dev-)check`.  For instance the keys
  `dirac-gcc.check` and `dirac-gcc.dev-check`.
* Since keys omit any time or sequence info, a rerun can/will overwrite a
  previous failure.  This is useful to deal with (fairly rare) timeouts or
  other external factors which may cause a build to fail.
    * The GitLab UI has a "Retry" button of one form or another on several of
      the pages which display job status.  On dashboard-type views it is the
      icon consisting of two arrows forming a circle.
* For `(branch =~ '^[user]/*')`, with `[user]` any sequence of non-/ characters,
  then build results are reported to `https://bitbucket.org/[user]/[repo]`
    * The value of `[repo]` defaults to `upcxx` with per-user exceptions
      handled by logic in the `gitlab-ci.yml`.
    * To allow build results to be posted to your fork, you'll need to invite
      the "UPCXX GitLab-CI" <upcxx.gitlab.ci@gmail.com> user to your repo with
      WRITE access, and email gasnet-staff@lbl.gov to request an admin accept
      the invitation.
    * As a consequence of this pattern rule, a user of `berkeleylab` can be
      used to report results to the main repo, to be visible in the BitBucket
      UI.  For example a push of commit `c3ac2cea` to a branch named
      `berkeleylab/dev-check/mk-develop-c3ac2cea` might be used to gather CI
      results for that commit.

### Manually triggering CI tests

One can trigger runs manually in a simple (stupid) UI accessible at
`https://ecpci-test.alcf.anl.gov/anl/upcpp/pipelines/new`

* Choose a branch from the pull down
* Set any variables desired (described below)
* Click "Run Pipeline"

### In addition to use of the UI, one can use query URLs via a browser

* `https://ecpci-test.alcf.anl.gov/anl/upcpp/pipelines/new?ref=develop&var[CI_HOST]=dirac`
   will (re)test `develop` with variable `CI_HOST` set to `dirac`
   (variables are described below).
* One must separate multiple variable settings with `&`
* CLI web clients don't work because MFA is required by the GitLab server
* Storing such links in a wiki, markdown or GDoc works fine

### CI variables

* The default is to run all jobs (job = host-compiler) for any commit
* The following "selection" variables (listed with their supported values)
  can be set in order to limit runs to the subset mating the constraints:
    * `CI_COMPILER`: gnu, llvm, pgi, intel, cray, xcode
    * `CI_AGE`:      new, old
    * `CI_CPU`:      x86_64, knl, ppc64le, aarch64
    * `CI_OS`:       linux, cnl, macos
    * `CI_HOST`:     theta, theta-fe, dirac
    * `CI_NAME`:     name of a specific job, such as "theta-PrgEnv_llvm_floor"
* The following control what is tested or how, with example legal values
    * `CI_DEV_CHECK`:      Must be 0 or 1 (or blank/unset for the default)
    * `CI_RUN_TESTS`:      Must be 0 or 1 (or blank/unset for the default)
    * `CI_NETWORKS`:       smp udp (blank is erroneous, unset for defaults)
    * `CI_RANKS`:          4
    * `CI_MAKE`:           /usr/local/bin/make
    * `CI_MAKE_PARALLEL`:  -j6
    * `CI_CONFIGURE_ARGS`: Arguments to append to the configure command line
    * `CI_EXTRAFLAGS`:     Sets make variable EXTRAFLAGS when building tests.
                           Default is "-Werror"
    * `CI_TESTS`:          Sets make variable TESTS when building tests.
                           Default is unset
    * `CI_NO_TESTS`:        Sets make variable NO_TESTS when building tests
                            Default is unset
* The following per-host/per-job variables provide additional fine-grained
  control:
    * `CI_THETA_RUN_ALL_PRGENV`:
        * Defaults to empty.  Only theta-PrgEnv_llvm{,_floor} run their tests.
        * Set to '1' to enable *all* theta-PrgEnv_* jobs to submit the tests
          to be run on the compute nodes.
* Any variable set when requesting the run is propagated to the environment.
    * For instance, one might set `UPCXX_RUN_TIME_LIMIT` for particularly slow
      testers, or `TEST_CERR_LIMIT` to truncate error output.  
      Additionally, the `TEST_{FLAGS,ARGS,ENV}_[TESTNAME]` variables are
      available to add any extra compiler flags or to control the run command
      line and environment.  However, this is somewhat fragile, since these
      are not additive with respect to default values or ones set within the
      CI infrastructure.

### Modifying gitlab-ci.yml and dev-ci scripts

* The following variables (with their defaults) control where dev-ci scripts
  are found:
    * `CI_UPCXX_CI_REPO`:    https://bitbucket.org/berkeleylab/upcxx-ci.git
    * `CI_UPCXX_CI_BRANCH`:  master

Note that the named repository must be public.
Use of `ssh:` URLs is not supported.

Use of these settings alone is sufficient to cause the `gitlab-ci.yml` for
fetch the `dev-ci` scripts from a non-default location.  When modifying only
those scripts, setting those variables via UI or request URL is sufficient.

To test modifications to `gitlab-ci.yml` one must first create a branch of the
`upcxx` repo to specify the alternate `.yml` file.  Committing the following
four-line `gitlab-ci.yml` to a branch of the `upcxx` repo (with proper
substitutions for `[USER]` and `[BRANCH]`) will ensure CI testing uses an
alternative branch of `upcxx-ci` for both the `gitlab-ci.yml` and the `dev-ci`
scripts:

```yaml
include: 'https://bitbucket.org/[USER]/upcxx-ci/raw/[BRANCH]/gitlab-ci.yml'
variables:
  CI_UPCXX_CI_REPO: "https://bitbucket.org/[USER]/upcxx-ci.git"
  CI_UPCXX_CI_BRANCH: "[BRANCH]"
```
